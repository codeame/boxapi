# -*- coding: utf-8 -*-



""" 
theBoxAPI.
http://www.codea.me/theBox 


(c) 2012 by Miguel Chavez Gamboa
miguel@codea.me 

License: BSD
"""


from eve import Eve

#for token generation.
import bcrypt

#for logging
import logging


#for Authentication
from eve.auth import TokenAuth
#for custome validation.
from eve.io.mongo import Validator


import datetime

class clockDateConsistencyValidator(Validator):
    def _validate_checkDateConsistency(self, checkInDateConsistency, field, value):
        ''' Validates if the in_datetime datetime is consistent. This means:
                * Only one clock-in/clock-out per day [does not matters time] 
                * in_datetime < out_datetime
                * Open clock-ins (which still not clock-out) have the same in_datetime as out_datetime.

            It is still left out the individual clock in/out process validation, here only simple consistency validation.
            At ClientApp, the logic validation must happen (check if there is an open clock-in for doing a clock-out).
        '''
        # Getting data from the document.
        userid = self.document['userid']
        in_datetime = self.document['in_datetime']
        out_datetime = self.document['out_datetime']
        # Getting the date only from the in_datetime
        #in_datetime_dateonly = in_datetime.date()
        in_datetime_dateonly = datetime.datetime(in_datetime.year, in_datetime.month, in_datetime.day, 0, 0, 0, 0)
        # Getting all the clocks for the userid.
        # Limit clocks for later or equal to the in_datetime. The result should be ONE or ZERO records.
        clocks = app.data.driver.db.clocks.find( {'in_datetime': {'$gte': in_datetime_dateonly}, 'userid':userid }  )

        # Validating Clock-in: Only one clock-in per day.
        if clocks.count() >= 1:
            self._error("Already Clocked-in. Only ONE clock-in per day allowed.")


        # Validating clock-out: in_datetime < out_datetime. in = out means an open clock-in.
        if in_datetime < out_datetime:
            self._error("Clock-out must be later than clock-in. (clock-out > clock-in)")



class RolesAuth(TokenAuth):
    def check_auth(self, token,  allowed_roles, resource, method):
        users = app.data.driver.db['users']
        lookup = {'token': token}
        if allowed_roles:
            # only retrieve a user if his roles match ``allowed_roles``
            lookup['roles'] = {'$in': allowed_roles}
        user = users.find_one(lookup)
        return user


def add_token(documents):
    for document in documents:
        usr = document["username"]+document["firstname"]+document["lastname"]
        token = bcrypt.hashpw(usr, bcrypt.gensalt(10))
        document["token"] = token
        app.logger.debug('Creating token %s for user %s', token, document['username'])
        

if __name__ == '__main__':
    port = 5000
    host = '127.0.0.1'

    #app = Eve(auth=RolesAuth)
    app = Eve(validator=clockDateConsistencyValidator)

    #logger
    file_handler = logging.FileHandler('theBoxAPI.log')
    app.logger.addHandler(file_handler)
    app.logger.setLevel(logging.DEBUG)

    #Add a hook on users creation.
    app.on_insert_users += add_token

    app.run(host=host, port=port)
