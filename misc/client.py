# -*- coding: utf-8 -*-

"""
    eve-demo-client
    ~~~~~~~~~~~~~~~

    Simple and quickly hacked togheter, this script is used to reset the
    eve-demo API to its initial state. It will use standard API calls to:

        1) delete all items in the 'people' and 'works' collections
        2) post multiple items in both collection

    I guess it can also serve as a basic example of how to programmatically
    manage a remot e API using the phenomenal Requests library by Kenneth Reitz
    (a very basic 'get' function is included even if not used).

    :copyright: (c) 2012 by Nicola Iarocci.
    :license: BSD, see LICENSE for more details.
"""
import requests
import json
import random

import datetime
from bson import json_util

#ENTRY_POINT = 'http://eve-demo.herokuapp.com'
ENTRY_POINT = 'http://127.0.0.1:5000'


def post_people():
    users = [
        {
            'username': 'admin',
            'firstname': 'Administrator',
            'lastname': '.',
            'role': ['admin'],
            'token': 'ASDF',
            'pin': '1234'

        },
        {
            'username': 'device',
            'firstname': 'None',
            'lastname': '.',
            'role': ['superuser'],
            'token': 'QWERTY',
            'pin': '0987'
        },
    ]

    payload = {}
    for person in users:
        payload[person['username']] = json.dumps(person)

    r = perform_post('users', payload)
    print "'user' posted"
    print r.status_code
    print r.json

    valids = []
    if r.status_code == 200:
        response = r.json()
        print response
        for person in payload:
            result = response[person]
            if result['status'] == "OK":
                valids.append(result['_id'])

    print valids
    return valids


def post_clocks(ids):
    dt = datetime.datetime.utcnow()
    clocks = [
        {
            'userid': '52562258418694469c77bb43', #ids[0],
            'in_datetime': dt.strftime("%a, %d %b %Y %H:%M:%S GMT"),
            'out_datetime': dt.strftime("%a, %d %b %Y %H:%M:%S GMT")
        },
        {
            'userid': '52562258418694469c77bb44', #ids[1],
            'in_datetime': dt.strftime("%a, %d %b %Y %H:%M:%S GMT"),
            'out_datetime': dt.strftime("%a, %d %b %Y %H:%M:%S GMT")
        },
    ]
    

    payload = {}
    for clock in clocks:
        print 'SENDING CLOCK: %s'%clock
        payload[clock['userid']] = json.dumps(clock, default=json_util.default)
    r = perform_post('clocks', payload)
    print "'clocks' posted"
    print r.status_code
    print r.json

    valids = []
    if r.status_code == 200:
        response = r.json()
        print 'RESPONSE: %s'%response
        for clock in payload:
            result = response[clock]
            print 'RESULT: %s'%result
            if result['status'] == "OK":
                valids.append(result['_id'])

    print valids


def perform_post(resource, data):
    return requests.post(endpoint(resource), data)


def delete():
    print '...DELETING....'
    #r = perform_delete('users')
    #print "'users' deleted"
    #print r.status_code
    #r = perform_delete('clocks')
    #print "'clocks' deleted"
    #print r.status_code


def perform_delete(resource):
    return requests.delete(endpoint(resource))


def endpoint(resource):
    return '%s/%s/' % (ENTRY_POINT, resource)


def get():
    r = requests.get('http://127.0.0.1:5000')
    print r.json

if __name__ == '__main__':
    delete()
    ids = post_people()
    post_clocks(ids)
    #get()