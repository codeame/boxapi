# -*- coding: utf-8 -*-
"""
theBoxAPI.
http://www.codea.me/theBox 


(c) 2012 by Miguel Chavez Gamboa
miguel@codea.me 

License: BSD
"""


MONGO_HOST = 'localhost'
MONGO_PORT = 27017
MONGO_USERNAME = 'boxAdmin'
MONGO_PASSWORD = 'qwerty'
MONGO_DBNAME = 'theBox'

# let's not forget the API entry point
SERVER_NAME = '127.0.0.1:5000'




# Enable reads (GET), inserts (POST) and DELETE for resources/collections
# (if you omit this line, the API will default to ['GET'] and provide
# read-only access to the endpoint).
RESOURCE_METHODS = ['GET', 'POST', 'DELETE']

# Enable reads (GET), edits (PATCH) and deletes of individual items
# (defaults to read-only item access).
ITEM_METHODS = ['GET', 'PATCH', 'DELETE']

# We enable standard client cache directives for all resources exposed by the
# API. We can always override these global settings later.
CACHE_CONTROL = 'max-age=20'
CACHE_EXPIRES = 20



users = {
    'additional_lookup': {
        'url': '[\w]+',
        'field': 'username'
    },
    
    # We disable endpoint caching as we don't want client apps to cache users data.
    'cache_control': '',
    'cache_expires': 0,

    # Only allow superusers and admins.
    'allowed_roles': ['superuser', 'admin', 'device'],

    # Allow 'token' to be returned with POST responses
    # NOTE: Really do this?... study more deeply this!.
    'extra_response_fields': ['token'],

    'schema': {
        'username': {
            'type': 'string',
            'minlength': 1,
            'maxlength': 50,
            'required': True,
            'unique': True,    #due to the fact that it is an entry point
        },
        'pin': {
            'type': 'string',
            'required':True,
        },
        'token': {
             'type': 'string',
             'required': True,
         },
        'firstname': {
            'type': 'string',
            'minlength': 1,
            'maxlength': 50,
            'required': True,
        },
        'lastname': {
            'type': 'string',
            'minlength': 1,
            'maxlength': 50,
            'required': True,
        },
        # 'role' is a list, and can only contain values from 'allowed'.
        'role': {
            'type': 'list',
            'allowed': ["clerk", "device", "supervisor", "admin", "superuser"],
            'required': True,
        },
    }
}


clocks = {
    'schema': {
        'userid': {
            'type': 'objectid',
            'required': True,
            'unique': False,
            'data_relation': {
                'collection': 'users',
                'field': '_id',
                'embeddable': True
            },
        },
        'in_datetime': {
            'type': 'datetime',
            'required': True,
            'checkDateConsistency':True
        },
        'out_datetime':{
            'type': 'datetime',
            'required': False,
            'default': None,
        },
    }
}




DOMAIN = {
    'users': users,
    'clocks': clocks,
}


